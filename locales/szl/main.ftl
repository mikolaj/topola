tr-menu-file = Zbiōr
tr-menu-file-open = Ôtwōrz
tr-menu-open-specctra-session-file = Zbiōr sesyje Specctra (*.ses)
tr-menu-open-specctra-design-file = Zbiōr projektu Specctra (*.dsn)
tr-menu-file-export-session-file = Eksportuj zbiōr sesyje
tr-menu-file-import-history = Importuj historyjõ
tr-menu-file-export-history = Eksportuj historyjõ
