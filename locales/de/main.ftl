tr-menu-edit-undo = Rückgängig
tr-menu-edit-redo = Vorwärts
tr-menu-view-show-layer-manager = Zeige Ebenen-Verwaltung
tr-menu-open-specctra-session-file = Specctra-Sitzungsdatei (*.ses)
tr-menu-view-frame-timestep = Rahmen-Zeitschritt
tr-menu-file = Datei
tr-menu-edit = Bearbeiten
tr-menu-view = Ansicht
tr-menu-place = Platzieren
tr-menu-route = Route
tr-menu-inspect = Inspizieren
tr-menu-options = Optionen
tr-menu-file-open = Öffnen
tr-menu-file-export-session-file = Sitzungsdatei exportieren
tr-menu-file-import-history = Historie importieren
tr-menu-file-export-history = Historie exportieren
tr-menu-file-quit = Beenden
tr-menu-route-autoroute = Autorouten
tr-menu-place-place-via = Platziere DuKo
tr-menu-edit-remove-bands = Bänder entfernen
tr-menu-inspect-compare-detours = Vergleiche Umwege
tr-menu-route-options-presort-by-pairwise-detours = Nach paarweisen Umwegen vorsortieren
tr-menu-route-options-squeeze-through-under-bends = Presse unter Biegungen durch
tr-menu-route-options-wrap-around-bands = Wickle um Bänder
tr-menu-view-zoom-to-fit = Einpassen
tr-menu-view-show-ratsnest = Zeige Ratsnest
tr-menu-view-show-navmesh = Zeige Navmesh
tr-menu-view-show-bboxes = Zeige BBoxen
tr-menu-view-show-origin-destination = Zeige Ursprung-Ziel
tr-menu-inspect-measure-length = Länge messen
tr-error-failed-to-parse-as-specctra-dsn = kann Datei nicht als Specctra-DSN interpretieren
tr-module-history-file-loader = Historiendateilader
tr-menu-edit-abort = Ablehnen
tr-dialog-error-messages = Fehlermeldungen
tr-dialog-error-messages-reset = Meldungen zurücksetzen
tr-dialog-error-messages-discard = Verwerfen
tr-module-specctra-dsn-file-loader = Specctra DSN Dateilader
tr-module-invoker = Aufrufer
tr-error-unable-to-read-file = kann Datei nicht lesen
tr-error-failed-to-parse-as-history-json = kann Datei nicht als Historien-JSON interpretieren
tr-error-unable-to-initialize-overlay = kann Overlay nicht initialisieren
tr-error-unable-to-initialize-autorouter = kann Autorouter nicht initialisieren
tr-menu-help = Hilfe
tr-menu-help-online-documentation = Onlinedokumentation
tr-menu-preferences = Einstellungen
tr-menu-preferences-set-language = Sprache wählen
tr-menu-route-routed-band-width = Routen-Bandbreite
tr-menu-edit-select-all = Alles auswählen
tr-menu-edit-unselect-all = Alles abwählen
tr-menu-open-specctra-design-file = Specctra-Designdatei (*.dsn)
tr-menu-edit-recalculate-topo-navmesh = Topologisches Navmesh neuberechnen
tr-menu-view-show-topo-navmesh = Topologisches Navmesh anzeigen
