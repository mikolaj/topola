{
  "done": [
    {
      "Autoroute": [
        [
          {
            "pin": "J2-2",
            "layer": "F.Cu"
          },
          {
            "pin": "J2-1",
            "layer": "F.Cu"
          },
          {
            "pin": "D3-2",
            "layer": "F.Cu"
          },
          {
            "pin": "D2-1",
            "layer": "F.Cu"
          },
          {
            "pin": "D3-1",
            "layer": "F.Cu"
          },
          {
            "pin": "D1-1",
            "layer": "F.Cu"
          },
          {
            "pin": "J1-1",
            "layer": "F.Cu"
          },
          {
            "pin": "D2-2",
            "layer": "F.Cu"
          },
          {
            "pin": "D1-2",
            "layer": "F.Cu"
          },
          {
            "pin": "D4-1",
            "layer": "F.Cu"
          },
          {
            "pin": "J1-2",
            "layer": "F.Cu"
          }
        ],
        {
          "presort_by_pairwise_detours": false,
          "router_options": {
            "wrap_around_bands": true,
            "squeeze_through_under_bends": true,
            "routed_band_width": 100.0
          }
        }
      ]
    },
    {
      "Autoroute": [
        [
          {
            "pin": "D4-2",
            "layer": "F.Cu"
          },
          {
            "pin": "J2-2",
            "layer": "F.Cu"
          }
        ],
        {
          "presort_by_pairwise_detours": false,
          "router_options": {
            "wrap_around_bands": true,
            "squeeze_through_under_bends": true,
            "routed_band_width": 100.0
          }
        }
      ]
    }
  ],
  "undone": []
}
