{
  "done": [
    {
      "Autoroute": [
        [
          {
            "pin": "L1-2",
            "layer": "F.Cu"
          },
          {
            "pin": "L1-1",
            "layer": "F.Cu"
          },
          {
            "pin": "D1-1",
            "layer": "F.Cu"
          },
          {
            "pin": "D1-2",
            "layer": "F.Cu"
          },
          {
            "pin": "C1-2",
            "layer": "F.Cu"
          },
          {
            "pin": "R1-1",
            "layer": "F.Cu"
          },
          {
            "pin": "C1-1",
            "layer": "F.Cu"
          },
          {
            "pin": "R1-2",
            "layer": "F.Cu"
          }
        ],
        {
          "presort_by_pairwise_detours": false,
          "router_options": {
            "routed_band_width": 100.0,
            "wrap_around_bands": true,
            "squeeze_through_under_bends": true
          }
        }
      ]
    }
  ],
  "undone": []
}